module.exports = {
  replaceText: function (needle, haystack, value) {
    let pat = new RegExp("\\{\\{" + needle + "\\}\\}", "g");
    return haystack.replace(pat, value);
  },

  postMessage: function (request, response) {
    const sgMail = require("@sendgrid/mail");

    const key = process.env.SENDGRID_API_KEY;

    sgMail.setApiKey(key);
    let email = request.body.emailAddress;
    let subject = request.body.subject;
    let text = request.body.textMessage;
    let html = request.body.htmlMessage;

    html = this.replaceText("email", html, email);
    html = this.replaceText("server", html, "http://" + request.headers.host);
    text = this.replaceText("email", text, email);
    text = this.replaceText("server", text, "http://" + request.headers.host);

    //   const msg = {
    //     to: "allynswildflourshoppe@gmail.com",
    //     from: "noreply@newbreadorder.com",
    //     subject: "New Bread Order",
    //     text: text,
    //     html: html
    //   };

    const msg = {
      to: "allynswildflourshoppe@gmail.com",
      from: "noreply@newbreadorder.com",
      subject: "New Bread Order",
      text: "nskdnfsd",
      html: "<h1>hi</h1>",
    };

    let allGood = true;

    if (allGood) {
      sgMail
        .send(msg)
        .then(function (value) {
          response.json({ response: "good" });
        })
        .catch(function (error) {
          response.json({ response: "bad", error: error });
          console.log(key);
        });
    } else {
      response.json({ response: "bad", error: "This user opted out" });
    }
  },
};
